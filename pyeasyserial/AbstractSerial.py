class AbstractSerial:

    def __del__(self):
        self.close()

    def close(self) -> None:
        pass

    def read(self) -> str:
        pass

    def open(self, **kwargs) -> str:
        pass

    def write(self, output: str) -> None:
        pass
