import logging
import serial
from serial import Serial
from .AbstractSerial import AbstractSerial


class SerialPort(AbstractSerial):
    """
    Wrapper for pyserial
    """
    __serial: Serial
    __encoding: str

    def __init__(self, encoding: str = 'utf8'):
        self.__encoding: str = encoding

    def close(self):
        self.__serial.close()

    def read(self) -> str:
        try:
            return self.__serial.readline().decode(self.__encoding).strip()
        except Exception as exception:
            self.__capture_error(exception)

    def write(self, output: str) -> None:
        try:
            self.__serial.write(bytes(output, encoding=self.__encoding))
        except Exception as exception:
            self.__capture_error(exception)

    @staticmethod
    def __capture_error(error: Exception) -> None:
        logging.error(str(error))
        raise error

    def open(self, **kwargs) -> None:
        """
        :param kwargs:
            port : (str)Device name
            baud_rate: (int) baud rate
            read_timeout: (float) read timeout
            write_timeout: (float) write timeout
        """
        port: str = kwargs.get('port')
        baud_rate: int = kwargs.get('baud_rate', 9600)
        read_timeout: float = kwargs.get('read_timeout', None)
        write_timeout: float = kwargs.get('write_timeout', None)

        try:
            self.__serial = Serial(port=port,
                                   baudrate=baud_rate,
                                   timeout=read_timeout,
                                   write_timeout=write_timeout)
            logging.info("Serial port (" + port + ") connected.")
        except Exception as exception:
            self.__capture_error(exception)
