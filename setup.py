from setuptools import setup, find_packages

setup(name='pyeasyserial',
      packages=find_packages(),
      version='0.1.0.20210826',
      description='Wrapper to facilitate device-to-device serial communication',
      keywords=["serial port", "serial", "port", "device-to-device", "wrapper", "python"],
      author='BeLittleMaker',
      author_email='belittlemaker@gmail.com',
      url='https://gitlab.com/belittlemaker/py-easy-serial',
      license='GNU',
      install_requires=['pyserial'],
      classifiers=["Programming Language :: Python :: 3.8"])
