# py-easy-serial

Wrapper to facilitate device-to-device serial communication. First approach for serial port.

Install package
--------------

* Linux

```
python3 -m pip install git+https://gitlab.com/belittlemaker/py-easy-serial.git@master
```

* Windows

```
py -m pip install git+https://gitlab.com/belittlemaker/py-easy-serial.git@master
```
